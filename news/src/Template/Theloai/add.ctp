<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Theloai $theloai
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Theloai'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="theloai form large-9 medium-8 columns content">
    <?= $this->Form->create($theloai) ?>
    <fieldset>
        <legend><?= __('Add Theloai') ?></legend>
        <?php
            echo $this->Form->control('Ten');
            echo $this->Form->control('TenKhongDau');
            echo $this->Form->control('created_at');
            echo $this->Form->control('updated_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
