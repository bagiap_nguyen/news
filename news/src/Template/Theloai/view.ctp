<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Theloai $theloai
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Theloai'), ['action' => 'edit', $theloai->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Theloai'), ['action' => 'delete', $theloai->id], ['confirm' => __('Are you sure you want to delete # {0}?', $theloai->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Theloai'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Theloai'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="theloai view large-9 medium-8 columns content">
    <h3><?= h($theloai->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Ten') ?></th>
            <td><?= h($theloai->Ten) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('TenKhongDau') ?></th>
            <td><?= h($theloai->TenKhongDau) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($theloai->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($theloai->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated At') ?></th>
            <td><?= h($theloai->updated_at) ?></td>
        </tr>
    </table>
</div>
