<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?= $cakeDescription ?>:
            <?= $this->fetch('title') ?>
        </title>
        <?= $this->Html->meta('icon') ?>

        <?= $this->Html->css(['bootstrap.min.css','shop-homepage.css','my.css']) ?>
 

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Tin Tức</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li>
                          <!--  <a href="pages/introduct">Giới thiệu</a> -->
                            <?php  
                                echo $this->Html->link(
                                    'Giới thiệu ',
                                    ['controller' => 'Pages','action' => 'introduct','_full' => true ]
                                );
                            ?>
                        </li>
                        <li>
                            <!--<a href="pages/contact">Liên hệ</a>-->
                            <?php  
                                echo $this->Html->link(
                                    'Liên hệ',
                                    ['controller' => 'Pages','action'=>'contact','_full'=>true]
                                );
                            ?>
                        </li>
                    </ul>

                    <form class="navbar-form navbar-left" role="search">
                        <div class="form-group">
                          <input type="text" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>

                    <ul class="nav navbar-nav pull-right">
                        <li>
                            <!--<a href="dangki.html">Đăng ký</a> -->
                            <?php  
                                echo $this->Html->link(
                                    'Đăng ký',
                                    ['controller' => 'Users', 'action' => 'register', '_full' => true]
                                );
                            ?>
                        </li>
                        <li>
                            <!--<a href="dangnhap.html">Đăng nhập</a>-->
                            <?php  
                                echo $this->Html->link('Đăng nhập',['controller' => 'Users','action' =>'login','_full' => true]);
                            ?>
                        </li>
                        <li>
                            <a>
                                <span class ="glyphicon glyphicon-user"></span>

                            </a>
                        </li>

                        <li>
                            <a href="#">Đăng xuất</a>
                        </li>

                    </ul>
                </div>



                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>

        <?= $this->Flash->render() ?>


        <div class="container clearfix">
            <?= $this->fetch('content') ?>
        </div>


        <footer>
            <div class="row">
                <div class="col-md-12">
                    <p align="center">Copyright &copy; Your Website 2014</p>
                </div>
            </div>
        </footer>

        <?php echo $this->Html->script(['jquery.js','bootstrap.min.js','my.js']) ?>
    </body>
</html>
