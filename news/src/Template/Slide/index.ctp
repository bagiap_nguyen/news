<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Slide[]|\Cake\Collection\CollectionInterface $slide
 */
?>
<!-- slider -->


        <div class="row carousel-holder">
            <div class="col-md-12">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>

                    <div class="carousel-inner">
                        <?php  
                            foreach ($slide as $item) :
                                if ($item->id == 1) :
                        ?>
                        <div class="item active">
                            <img class="slide-image" src="slide/<?= h($item->Hinh); ?>" alt="">
                        </div>
                        <?php  
                                else:
                        ?>
                        <div class="item">
                            <img class="slide-image" src="slide/<?= h($item->Hinh);?>" alt="">
                        </div>
                        <?php  
                                endif;
                            endforeach;
                        ?>
                        <div class="item">
                            <img class="slide-image" src="image/800x300.png" alt="">
                        </div>

                        

                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
            </div>
        </div>
        <!-- end slide -->