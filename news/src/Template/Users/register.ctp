

<!-- Page Content -->
    <div class="container">

    	<!-- slider -->
    	<div class="row carousel-holder">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
				  	<div class="panel-heading">Đăng ký tài khoản</div>
				  	<div class="panel-body">
				  		<?= $this->Flash->render() ?>
				    	<?= $this->Form->create() ?>
				    	<fieldset>
				    		<div>
							  	<?php 
							  	  echo $this->Form->control('Họ tên',[
							  	  			'name' =>'name',
							  				'type' =>'text',
							  				'class'=>'form-control',
							  				'placeholder' => 'Username',
							  				'aria-describedby' => 'basic-addon1',
							  		]);	
							  	?>
							</div>
							<br>
							<div>
				    			<?php 
							  	  echo $this->Form->control('Email',[
							  	  			'name' =>'email',
							  				'type' =>'email',
							  				'class'=>'form-control',
							  				'placeholder' => 'Email',
							  				'aria-describedby' => 'basic-addon1'
							  		]);	
							  	?>
							</div>
							<br>	
							<div>
				    			<!--<label>Nhập mật khẩu</label>
							  	<input type="password" class="form-control" name="password" aria-describedby="basic-addon1">-->
							  	<?php 
							  	  echo $this->Form->control('Nhập mật khẩu',[
							  	  			'name' =>'password',
							  				'type' =>'password',
							  				'class'=>'form-control',
							  				'aria-describedby' => 'basic-addon1'
							  		]);	
							  	?>
							</div>
							<br>
							<div>
				    			<!--<label>Nhập lại mật khẩu</label>
							  	<input type="password" class="form-control" name="passwordAgain" aria-describedby="basic-addon1">-->
							  	<?php 
							  	  echo $this->Form->control('Nhập lại mật khẩu',[
							  	  			'name' =>'passwordAgain',
							  				'type' =>'password',
							  				'class'=>'form-control',
							  				'aria-describedby' => 'basic-addon1'
							  		]);	
							  	?>
							</div>
							<br>
							</fieldset>
							<!--<button type="button" class="btn btn-success">Đăng ký
							</button>-->
							<?= $this->Form->button('Đăng ký' ,[
									'class' => 'btn btn-success',
									'type' => 'submit'
								]) ?>

				    	<?= $this->Form->end() ?>
				  	</div>
				</div>
            </div>
            <div class="col-md-2">
            </div>
        </div>
        <!-- end slide -->
    </div>
    <!-- end Page Content -->