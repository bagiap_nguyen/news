<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Tintuc[]|\Cake\Collection\CollectionInterface $tintuc
 */
?>

<!-- Page Content -->
    <div class="container">

        <!-- slider -->
        <?php  
            if (!is_null($slide) && count($slide) > 0) :
        ?>
        <div class="row carousel-holder">
            <div class="col-md-12">

                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">

                        <?php 

                            foreach ($slide as $img) :
                                if ($img->id == 1) :
                        ?>
                        <div class="item active">
                            <!--<img class="slide-image" src="image/800x300.png" alt="">-->
                            <?php  
                                echo $this->Html->image("slide/{$img->Hinh}",[
                                        'class' =>'slide-image',
                                        'alt'   => '',
                                        'width' =>'800px',
                                        'height'=> '300px',
                                        'url'   => '#'
                                ])
                            ?>
                        </div>
                        <?php  
                                else:
                        ?>
                        <div class="item">
                            <!--<img class="slide-image" src="image/800x300.png" alt="">-->
                            <?php  
                                echo $this->Html->image("slide/{$img->Hinh}",[
                                                            'class' => 'slide-image',
                                                            'width' =>'800px',
                                                            'height'=> '300px',
                                                            'url'   => '#'
                                                        ]);
                            ?>
                        </div>
                        <?php  
                                    endif;
                                endforeach;
                        ?>
                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
            </div>
        </div>
        <?php  
            endif;
        ?>
        <!-- end slide -->

        <div class="space20"></div>


        <div class="row main-left">
            <div class="col-md-3 ">
                <ul class="list-group" id="menu">

                    <li href="#" class="list-group-item menu1 active">
                        Menu
                    </li>
                    
                    <?php  
                    	foreach ($menu as $item) :
                    ?>

                    <li href="#" class="list-group-item menu1">
                        <?php  
                        	echo $item['Ten'];
                        ?>
                    </li>
                    	
                    <ul>
                    	<?php  
                			foreach ($menu2 as $childMenu) :
                				if ($item->id == $childMenu->idTheLoai) :
                		?>
                    		<li class="list-group-item">
                            	<a href="loaitin/index">
	                            	<?php  
	                            		echo $childMenu->Ten;
	                            	?>
                            	</a>
                            </li>
                        <?php  
                        		endif;
                			endforeach;
                        ?>    
                    </ul>
                    	
                    <?php  		
                    	endforeach;
                    ?>
                  
                </ul>
            </div>

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color:#337AB7; color:white;" >
                        <h2 style="margin-top:0px; margin-bottom:0px;"> Tin Tức</h2>
                    </div>

                    <div class="panel-body">
                        <!-- item -->
                        <?php  
                        	if (!is_null($tintuc) && count($tintuc) > 0) :
                        		foreach ($tintuc as $item) :

                        ?>
                        <div class="row-item row">
                        	<!--
                            <h3>
                                <a href="#">Category</a> |
                                <small><a href="loaitin.html"><i>subtitle</i></a>/</small>
                                <small><a href="loaitin.html"><i>subtitle</i></a>/</small>
                                <small><a href="loaitin.html"><i>subtitle</i></a>/</small>
                                <small><a href="loaitin.html"><i>subtitle</i></a>/</small>
                                <small><a href="loaitin.html"><i>subtitle</i></a>/</small>
                            </h3>-->
                            <div class="col-md-12 border-right">
                                <div class="col-md-3">
                                	<?php  
                                		echo $this->Html->image("tintuc/{$item->Hinh}", [
										    "alt" => "tintuc",
										    'url' => ['controller' 	=> 'Tintuc', 
										    			'action' 	=> 'view', 
										    			$item->id
										    		 ],
                                            'width'=> '130px'
										]);
                                	?>
			
                                </div>

                                <div class="col-md-9">
                                    <h3>
                                    	<?php  
                                    		echo $item->TieuDe;
                                    	?>
                                    </h3>
                                    <p>
                                    	<?php  
                                    		echo $item->TomTat;
                                    	?>
                                    </p>
                                    <a class="btn btn-primary" 
                                    	href="tintuc/view/<?php echo $item->id;  ?>">
                                    	Chi tiết.
                                    	<span class="glyphicon glyphicon-chevron-right">
                                    		
                                    	</span>
                                    </a>
                                </div>

                            </div>

                            <div class="break"></div>
                        </div>

                        <?php  
                        		endforeach;
                        	endif;
                        ?>
                        <!-- end item -->
                        <div class="paginator">
					        <ul class="pagination">
					            <?= $this->Paginator->first('<< ' . __('first')) ?>
					            <?= $this->Paginator->prev('< ' . __('previous')) ?>
					            <?= $this->Paginator->numbers() ?>
					            <?= $this->Paginator->next(__('next') . ' >') ?>
					            <?= $this->Paginator->last(__('last') . ' >>') ?>
					        </ul>
    					</div>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- end Page Content -->
