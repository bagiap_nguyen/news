<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Tintuc $tintuc
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Tintuc'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="tintuc form large-9 medium-8 columns content">
    <?= $this->Form->create($tintuc) ?>
    <fieldset>
        <legend><?= __('Add Tintuc') ?></legend>
        <?php
            echo $this->Form->control('TieuDe');
            echo $this->Form->control('TieuDeKhongDau');
            echo $this->Form->control('TomTat');
            echo $this->Form->control('NoiDung');
            echo $this->Form->control('Hinh');
            echo $this->Form->control('NoiBat');
            echo $this->Form->control('SoLuotXem');
            echo $this->Form->control('idLoaiTin');
            echo $this->Form->control('created_at');
            echo $this->Form->control('updated_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
