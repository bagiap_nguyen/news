<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Loaitin $loaitin
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $loaitin->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $loaitin->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Loaitin'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="loaitin form large-9 medium-8 columns content">
    <?= $this->Form->create($loaitin) ?>
    <fieldset>
        <legend><?= __('Edit Loaitin') ?></legend>
        <?php
            echo $this->Form->control('idTheLoai');
            echo $this->Form->control('Ten');
            echo $this->Form->control('TenKhongDau');
            echo $this->Form->control('created_at');
            echo $this->Form->control('updated_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
