<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Loaitin $loaitin
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Loaitin'), ['action' => 'edit', $loaitin->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Loaitin'), ['action' => 'delete', $loaitin->id], ['confirm' => __('Are you sure you want to delete # {0}?', $loaitin->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Loaitin'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Loaitin'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="loaitin view large-9 medium-8 columns content">
    <h3><?= h($loaitin->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Ten') ?></th>
            <td><?= h($loaitin->Ten) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('TenKhongDau') ?></th>
            <td><?= h($loaitin->TenKhongDau) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($loaitin->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('IdTheLoai') ?></th>
            <td><?= $this->Number->format($loaitin->idTheLoai) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($loaitin->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated At') ?></th>
            <td><?= h($loaitin->updated_at) ?></td>
        </tr>
    </table>
</div>
