<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tintuc Entity
 *
 * @property int $id
 * @property string $TieuDe
 * @property string $TieuDeKhongDau
 * @property string $TomTat
 * @property string $NoiDung
 * @property string $Hinh
 * @property int $NoiBat
 * @property int $SoLuotXem
 * @property int $idLoaiTin
 * @property \Cake\I18n\FrozenTime $created_at
 * @property \Cake\I18n\FrozenTime $updated_at
 */
class Tintuc extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'TieuDe' => true,
        'TieuDeKhongDau' => true,
        'TomTat' => true,
        'NoiDung' => true,
        'Hinh' => true,
        'NoiBat' => true,
        'SoLuotXem' => true,
        'idLoaiTin' => true,
        'created_at' => true,
        'updated_at' => true
    ];
}
