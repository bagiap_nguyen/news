<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tintuc Model
 *
 * @method \App\Model\Entity\Tintuc get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tintuc newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tintuc[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tintuc|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tintuc patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tintuc[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tintuc findOrCreate($search, callable $callback = null, $options = [])
 */
class TintucTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tintuc');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('TieuDe')
            ->maxLength('TieuDe', 255)
            ->requirePresence('TieuDe', 'create')
            ->notEmpty('TieuDe');

        $validator
            ->scalar('TieuDeKhongDau')
            ->maxLength('TieuDeKhongDau', 255)
            ->requirePresence('TieuDeKhongDau', 'create')
            ->notEmpty('TieuDeKhongDau');

        $validator
            ->scalar('TomTat')
            ->requirePresence('TomTat', 'create')
            ->notEmpty('TomTat');

        $validator
            ->scalar('NoiDung')
            ->maxLength('NoiDung', 4294967295)
            ->requirePresence('NoiDung', 'create')
            ->notEmpty('NoiDung');

        $validator
            ->scalar('Hinh')
            ->maxLength('Hinh', 255)
            ->requirePresence('Hinh', 'create')
            ->notEmpty('Hinh');

        $validator
            ->integer('NoiBat')
            ->requirePresence('NoiBat', 'create')
            ->notEmpty('NoiBat');

        $validator
            ->integer('SoLuotXem')
            ->requirePresence('SoLuotXem', 'create')
            ->notEmpty('SoLuotXem');

        $validator
            ->integer('idLoaiTin')
            ->requirePresence('idLoaiTin', 'create')
            ->notEmpty('idLoaiTin');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->dateTime('updated_at')
            ->allowEmpty('updated_at');

        return $validator;
    }
}
